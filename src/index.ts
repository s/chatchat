import "core-js/stable";
import "whatwg-fetch";

import * as FontFaceObserver from 'fontfaceobserver';

import * as PIXI from 'pixi.js';
import './assets/css/chatchat.css';

import * as PlayerIO from "./PlayerIOClient";

(async function() {
    let app = new PIXI.Application({width: 640, height: 600});
    document.body.appendChild(app.view);

    let font = new FontFaceObserver("Unscii");
    await font.load();

    let chatStyle = new PIXI.TextStyle({
        fontFamily: "Unscii",
        fontSize: 32,
        fill: "white",
    });

    let message = new PIXI.Text("Hello, world! β", chatStyle);
    message.position.set(32, 128);

    app.stage.addChild(message);

    const cli = await PlayerIO.authenticate(
        "kittygame-48om7qu7teeazkf9gana",
        "public",
        { userId: "aly" },
        []
    );
    console.log(cli);
    const multiplayer = cli.multiplayer;
    console.log(multiplayer);
    const rooms = await multiplayer.listRooms(
        "KittyRpg2",
        {},
        50,
        0
    )
    console.log(rooms);

    let messageN = new PIXI.Text("Hello, world! μ : (A ⊗ A) ⇝ A", chatStyle);
    messageN.position.set(32, 32);

    app.stage.addChild(messageN);
})();
