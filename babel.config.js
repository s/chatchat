module.exports = function (api) {
    api.cache(false);
    const presets = [
        [ "@babel/preset-typescript"],
        [
            "@babel/preset-env",
            {
                "corejs": { "version": "3.8" },
                "useBuiltIns": "usage",
                "targets": {
                    "edge": "15",
                    "firefox": "51",
                    "chrome": "56",
                    "safari": "11.1",
                    "ie": "11"
                }
            }
        ]
    ];
    const plugins = [
        "@babel/plugin-proposal-class-properties",
        [
            "@babel/plugin-transform-runtime",
            {
                "corejs": 3,
                "version": "^7.12.5",
            },
        ],
        "@babel/plugin-transform-arrow-functions",
    ];
    return {
        presets,
        plugins
    };
};
